// Function able to receive data without the use of global variables or prompt()

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Jungkook");
printName("Thonie");

// Data passed into the function through funtion invocation is called arguments
// The argument is then stored within a container called a parameter
function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

// check divisibility reusably using a function with arguments and parameter
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);
/*
	Mini-Activity

	1. Create a function which is cable to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console
	 2. Create a function which determine the input number is even number
	 	- This function should be able to receive a any number
	 	- Display the number and state if even number
 */

function faveHero(hero){
	console.log("My favorite hero is " + hero);
};

faveHero("Iron Man");

function checkEvenNum(evenNum){
	let remainder2 = evenNum % 2;
	let isDivisibleBy2 = remainder2 === 0;
	console.log("Is " + evenNum + " even number?" );
	console.log(isDivisibleBy2)
};

checkEvenNum(64);
checkEvenNum(27);

//Multiple Arguments can also be passed into a function; multiple parameters can contain our arguments

function printFullName(firstName, middleInitial, lastName){

	console.log(firstName + " " + middleInitial + " " + lastName);

};

printFullName('Juan', 'Crisostomo', 'Ibarra');

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more.less arguments than the expected parameters sometime causes an error or changes the behavior of the function
*/

printFullName('Stephen','Wardell');
printFullName('Stephen','Wardell', 'Curry', 'Thomas');
printFullName('Stephen','Wardell', 'Curry');

//Use variables as arguments
let fname = "Larry";
let mname = "Joe";
let lname = "Bird";

printFullName(fname, mname, lname);

/* Mini - Activity
	Create a function which will be able to receive 5 arguments
		- Receive 5 of your favorite songs
		- Display/print the passed 5 favorite songs in the console when the function is invoked.

*/
console.log("")

function printFavoriteSongs(song1, song2, song3, song4,song5){

	console.log("Here are my Top 5 Favorite Songs:")
	console.log("1. " + song1);
	console.log("2. " + song2);
	console.log("3. " + song3);
	console.log("4. " + song4);
	console.log("5. " + song5);

};

printFavoriteSongs("When I Met You","Crazy","Without You","Panalangin","Your Song");

// Return Statement
// Currently or so far, our function are able to display data in our console
// However, our function cannot yet return values. Function are able to return values which can be saved into a variable using the return statement/keyword.
 let fullName = printFullName("Gabrielle", "Vhernadette", "Fernandez");
 console.log(fullName);//undefined

 function returnFullName(firstName, middleName, lastName){
 	return firstName + ' ' + middleName + ' ' + lastName; 
 };

fullName = returnFullName("Juan", "Ponce", "Enrile");
//printFullName();return undefined because the function does not have return statement
//returnFullName();return a string as a value becuase it has a return statement
console.log(fullName);

// Functions which have a return statement are able to return value and it can be saved in a variable
console.log(fullName + ' is my gradpa.');

function returnPhilippineAddress(city){
	return city + ", Philippines.";
}; 

// return - return a values from a function which we can save in a variable
let myFullAddress = returnPhilippineAddress("San Pedro");
console.log(myFullAddress);

// returns true if number is divisible by 4, else returns false
function checkDivisibilityBy4(number){
	let remainder = number % 4;//remainder = 0
	let isDivisibleBy4 = remainder === 0;// true = 0 === 0
	// console.log(isDivisibleBy4);//true 
	console.log(remainder);

	// returns either true or false
	// Not only can you returnm raw values/data, you can also directly return a value
	// console.log(isDivisibleBy4);
	return remainder;//0
	// return isDivisibleBy4;//true
	// return keyword not only allows us to return value but also ends the process of the function
	console.log("I am run after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);//true
// let num14isDivisibleBy4 = checkDivisibilityBy4(14);
// num4isDivisibleBy4();
checkDivisibilityBy4(4);
console.log(checkDivisibilityBy4(4));//0
console.log(num4isDivisibleBy4);//true; 0
// console.log(num14isDivisibleBy4);

function createPlayerInfo(username, level, job){
	// console.log('username: ' + username + ' level ' + level + ' Job: ' + job);
	return('username: ' + username + ' level ' + level + ' Job: ' + job);
	console.log(1+1);
};

let user1 = createPlayerInfo('white_night', 95, 'Paladin');
console.log(user1);

/*	Mini-Activity
	Create a function which will be able to add two numbers
		- numbers must be provided as argument
		- display the result of the addition in our console
		- function should only display result. It should not return anything.

		- invoke and pass 2 arguments to the addition function
		- use return*/

function sum (num1,num2){
	return num1 + num2;
};

let sumOfNum = sum(1,4);
console.log(sumOfNum);

/*
	Create a function which will be able to multiply two numbers.
		- Numbers must be provided as arguments
		- Returm the result of multiplication

	Create a global variable called outside of the function called product
		- This product variable should be able to receive and store the result of multiplication function

	Log the value of product variable in the console

 */

 function productOfNum (num1,num2){
 	return num1 * num2;
 }

 let product = productOfNum(19,5);
 console.log(product);

 /*
  Create a function which will be able to get the area of a circle from a provided radius
     - a number should be provided as an argument
     - look up for the formula for calculating the area of a circle 
     - look up for the use of exponent operator
     - you can save the value of the calculations in a variable
     - return the result of the area 

   Create a global variable called outside of the function called circleArea
    - this variable should be able to receive and store the result of the calculation of area of a circle
   Log the value of the circleArea in the console.

 */

 function getCircleArea (radius){
 	let pi = 3.14159265359
 	return pi * radius **2;
 }

 let circleArea = getCircleArea(5);
 console.log(circleArea);

 /*--------------------ASSIGNMENT--------------------*/


function checkScorePercentage (yourScore, totalScore){
	let scorePercentage = (yourScore / totalScore) * 100

	return isPassed = scorePercentage >= 75
}

let checkIfPass = checkScorePercentage(90,100)
console.log(checkIfPass)